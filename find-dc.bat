@echo off
setlocal

:: Initialize the variable for domain suffix
set "dnsSuffix="

:: Retrieve the Primary DNS Suffix from ipconfig /all
for /f "tokens=2* delims=:" %%a in ('ipconfig /all ^| findstr /i "Primary DNS Suffix"') do (
    set "dnsSuffix=%%b"
)

:: Remove leading and trailing spaces
set "dnsSuffix=%dnsSuffix:~1%"
set "dnsSuffix=%dnsSuffix: =%"

:: Check if a domain suffix was found
if "%dnsSuffix%"=="" (
    echo Unable to find the domain name from ipconfig /all.
    echo Ensure that the "Primary DNS Suffix" is set in your network settings.
    exit /b 1
)

:: Display the detected domain name
echo Detected domain suffix: %dnsSuffix%

:: Resolve the domain name to its IP address
echo.
echo Resolving IP address for domain: %dnsSuffix%
for /f "tokens=2 delims=:" %%i in ('nslookup %dnsSuffix% ^| findstr /i "Address"') do (
    echo IP Address: %%i
)

:: Handle the case where no IP address was found
if "%IPAddress%"=="" (
    echo No IP address found for the domain: %dnsSuffix%
)

endlocal
